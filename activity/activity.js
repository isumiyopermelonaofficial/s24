//2. Finding users with the letter s in their first name or d in their last name.
db.users.find(
    
    
    {$or:[
    {firstName: {$regex: "S", $options: '$i'}},
    {lastName: {$regex: "D", $options: '$i'}}
    ]},
    
    {
        _id:0,
        firstName: 1,
        lastName: 1
    }
    
)

//3. Using the $and Operator
db.users.find(
    {$and: [
        {department:'HR'},
        {age: {$gt:70}}     
 ]}   
)

// 4. Find users with the letter e in their first name and has an age of less than or equal to 30
db.users.find(
    
    
    {$and:[
    {firstName: {$regex: "E", $options: '$i'}},
    {age: {$lte:30}}
    ]}    
)